const path = require('path')

module.exports = {
    module: {
        rules: [
            {
                exclude: [path.resolve(__dirname, 'node_modules')],
                test: /\.ts$/,
                use: 'ts-loader'
            },
            {
                exclude: [path.resolve(__dirname, 'node_modules')],
                test: /\.graphql?$/,
                use: [
                    {
                        loader: 'webpack-graphql-loader',
                        options: {
                            validate: true,
                            //schema: path.resolve(__dirname, 'src/schema.graphql')
                        }
                    }
                ]
            },
        ]
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.graphql', '.ts', '.js']
    },
    target: 'node'
}
