import { ApolloServer, gql } from 'apollo-server'
import { importSchema } from 'graphql-import'
import { makeExecutableSchema } from 'graphql-tools'
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import User from './models/User'
import Post from './models/Post'
import { resolvers } from './resolvers'

dotenv.config({ path: 'variables.env' })

const typeDefs = importSchema('src/schema.graphql')

const schema = makeExecutableSchema({ typeDefs, resolvers })

mongoose
    .connect(
        process.env.MONGO_URI,
        {
            useNewUrlParser: true,
            useCreateIndex: true
        }
    )
    .then(() => console.log('DB connected'))
    .catch((err: any) => console.error(err))

const server = new ApolloServer({
    schema,
    context: {
        User,
        Post
    }
})

server.listen().then(({ url }) => {
    console.log(`Server listening on ${url}`)
})