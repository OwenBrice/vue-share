export interface CreateUserInput {
    username: string;
    email: string;
    password: string
}

export interface AddPostInput {
    title: String;
    imageUrl: String;
    categories: [String];
    description: String;
    creatorId: String;
}
