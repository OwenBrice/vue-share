import { CreateUserInput, AddPostInput } from '../utils'

const Mutation = {
    signupUser: async (parent: any, args: CreateUserInput, context: any) => {
        const { User } = context
        const user = await User.findOne({ username: args.username })
        if (user) {
            throw new Error('User already exists')
        }

        const newUser = await new User({
            username: args.username,
            email: args.email,
            password: args.password
        }).save()

        return newUser
    },

    addPost: async (parent: any, args: AddPostInput, context: any) => {
        const newPost = await new context.Post({
            title: args.title,
            imageUrl: args.imageUrl,
            categories: args.categories,
            description: args.description,
            createdBy: args.creatorId
        }).save()

        return newPost
    }
}

export { Mutation }