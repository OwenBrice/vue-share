const Query = {
    getPosts: async (parent: any, args: any, context: any) => {
        const posts = await context.Post
            .find({})
            .sort({ createdDate: 'desc' })
            .populate({
                path: 'createdBy',
                ref: 'User'
            })

        return posts
    }
}

export { Query }