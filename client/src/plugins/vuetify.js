import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#0B3142',
    secondary: '#EFD6AC',
    accent: '#175676',
    error: '#e91e63',
    warning: '#ff5722',
    info: '#8bc34a',
    success: '#4caf50'
  }
})

/*
  primary: colors.teal.base,
  secondary: colors.cyan.base,
  accent: colors.red.base,
  error: colors.pink.base,
  warning: colors.deep - colors.orange.base,
  info: colors.light - colors.green.base,
  success: colors.green.base
*/

/*{
  primary: '#009688',
  secondary: '#00bcd4',
  accent: '#f44336',
  error: '#e91e63',
  warning: '#ff5722',
  info: '#8bc34a',
  success: '#4caf50'
}*/
